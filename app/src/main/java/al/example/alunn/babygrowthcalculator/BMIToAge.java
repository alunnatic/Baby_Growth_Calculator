package al.example.alunn.babygrowthcalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.commons.lang3.math.NumberUtils;

public class BMIToAge extends AppCompatActivity {

    //variables
    int m_f_flag = 1;
    EditText age_years;
    EditText age_months;
    EditText length_feet;
    EditText length_inches;
    TextView percentile_answer;
    TextView percentile_label;
    TextView percentile_10th;
    TextView percentile_90th;
    EditText weight_pounds;
    EditText weight_ounces;
    TextView bmi_output;
    private AdView mAdView5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmito_age);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //ad stuff
        mAdView5 = findViewById(R.id.adView_bmi_to_age);
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest request5 = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();
        mAdView5.loadAd(request5);


        weight_pounds = findViewById(R.id.value_pounds_bmi);
        weight_ounces = findViewById(R.id.value_ounces_bmi);
        age_years = findViewById(R.id.value_years_old_bmi);
        age_months = findViewById(R.id.value_months_old_bmi);
        length_feet = findViewById(R.id.value_feet_bmi);
        length_inches = findViewById(R.id.value_inches_bmi);
        percentile_answer = findViewById(R.id.value_output_percentile_bmi);
        percentile_label = findViewById(R.id.label_percentile_is_bmi);
        percentile_10th = findViewById(R.id.value_10th_percentile_bmi);
        percentile_90th = findViewById(R.id.value_90th_percentile_bmi);
        bmi_output = findViewById(R.id.value_output_BMI);

        //the toggle button that allows you to switch between male and female
        //it just sets "m_f_flag" to the appropriate value to get used in the onClick method for the "calculate" button
        ToggleButton m_f = findViewById(R.id.button_male_female_bmi);
        m_f.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    m_f_flag = 2;                               //toggle is enabled, switched to female
                } else {
                    m_f_flag = 1;                               //toggle is disabled, male (default)
                }
            }
        });

        //check input and send the correct values to the "CalculateAll" method
        Button calc = findViewById(R.id.button_calculate_bmi);
        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean wt_oz = NumberUtils.isParsable(weight_ounces.getText().toString());
                boolean wt_lbs = NumberUtils.isParsable(weight_pounds.getText().toString());
                boolean len_feet = NumberUtils.isParsable(length_feet.getText().toString());
                boolean len_inch = NumberUtils.isParsable(length_inches.getText().toString());
                boolean age_yrs = NumberUtils.isParsable(age_years.getText().toString());
                boolean age_mo = NumberUtils.isParsable(age_months.getText().toString());
                double age_in_months_decimal = 0;
                double inches = 0;
                double feet = 0;
                double ounces = 0;
                double pounds = 0;
                double bmi;


                //check if they entered months, if so convert months to a decimal value and add it
                //to the number of years. If no value for age in months, make the age equal to years only
                //if a valid number is in months
                if(age_mo){
                    //if a valid number is in years
                    if(age_yrs){
                        //if years entered is greater than or equal to 0
                        if(Double.parseDouble(age_years.getText().toString()) >= 0){
                            age_in_months_decimal = Double.parseDouble(age_years.getText().toString()) * 12;
                        }
                    }
                    //if months entered is greater than or equal to 0
                    if(Double.parseDouble(age_months.getText().toString()) >= 0){
                        double months_to_decimal = Double.parseDouble(age_months.getText().toString());
                        age_in_months_decimal += months_to_decimal;
                    }
                    //if no months entered, use years only
                }else if (age_yrs) {
                    if(Double.parseDouble(age_years.getText().toString()) >= 0)
                        age_in_months_decimal = Double.parseDouble(age_years.getText().toString()) * 12;
                }

                //if the age is out of range display a message to the user and exit the method so it doesn't crash
                if((age_in_months_decimal < 24) || (age_in_months_decimal > 240)){
                    percentile_label.setTextSize(10);
                    percentile_label.setText("Age must be between 2 and 20 years");
                    percentile_answer.setText("");
                    bmi_output.setText("");
                    return;
                }else {
                    percentile_label.setTextSize(24);
                    percentile_label.setText(getString(R.string.percentile_is ));
                }


                //if a valid number entered for inches
                if(len_inch){
                    inches = Double.parseDouble(length_inches.getText().toString());
                    if(inches < 0){
                        inches = 0;
                    }
                }

                //if a valid number entered for lbs
                if(len_feet){
                    feet = Double.parseDouble(length_feet.getText().toString());
                    if(feet < 0){
                        feet = 0;
                    }else {
                        feet = feet * 12;     //convert feet to inches
                    }
                }
                inches = feet + inches;
                //inches to centimeters
                inches = inches * 2.54;

                //if a valid number entered for ounces
                if(wt_oz){
                    ounces = Double.parseDouble(weight_ounces.getText().toString());
                    if(ounces >= 0){
                        ounces = ounces / 16;
                    }
                }

                //if a valid number entered for lbs
                if(wt_lbs){
                    pounds = Double.parseDouble(weight_pounds.getText().toString());
                    pounds += ounces;
                }
                //lbs to kg
                pounds = pounds * .45359237;
                bmi = pounds / inches / inches * 10000;
                TableDecision(age_in_months_decimal, bmi);
            }
        });
    }


    public void TableDecision(double formatted_age, double formatted_weight){
        int percentile;
        Table tbl = new Table(0, 0, 0, 0, 0, 0);
        TableMaker tM = new TableMaker();
        Calculations calc = new Calculations();

        //25 months until 20 years
        if(formatted_age >= 24 && formatted_age < 240){
            //25 months until 20 years male
            if(m_f_flag == 1){
                for (Table c : tM.CdcBoysBmiToAge()) {
                    if (formatted_age >= c.getAge() - .5 && formatted_age < c.getAge() + .5) {
                        tbl = c;
                    }
                }
            }
            //25 months until 20 years female
            if(m_f_flag == 2){
                for (Table d : tM.CdcGirlsBmiToAge()) {
                    if (formatted_age >= d.getAge() - .5 && formatted_age < d.getAge() + .5) {
                        tbl = d;
                    }
                }
            }
        }

        percentile = calc.CalculateTheZScores(formatted_weight, tbl.getM(), tbl.getS());
        percentile_answer.setText(Integer.toString(percentile));
        percentile_10th.setText(Float.toString(calc.GetReferencePercentileNoChange(tbl.getP10())));
        percentile_90th.setText(Float.toString(calc.GetReferencePercentileNoChange(tbl.getP90())));
        bmi_output.setText(Float.toString(calc.GetReferencePercentileNoChange(formatted_weight)));
    }
}
