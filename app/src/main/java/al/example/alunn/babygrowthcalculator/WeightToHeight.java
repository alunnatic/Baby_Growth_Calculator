package al.example.alunn.babygrowthcalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.commons.lang3.math.NumberUtils;

public class WeightToHeight extends AppCompatActivity {

    //variables
    boolean age_over_2 = false;
    int m_f_flag = 1;
    EditText weight_pounds;
    EditText weight_ounces;
    EditText feet;
    EditText inches;
    TextView percentile_answer;
    TextView percentile_10th;
    TextView percentile_90th;
    TextView percentile_label;
    private AdView mAdView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight_to_height);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //ad stuff
        mAdView3 = findViewById(R.id.adView_weight_to_height);
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest request3 = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();
        mAdView3.loadAd(request3);

        weight_pounds = findViewById(R.id.value_pounds_weight_to_height);
        weight_ounces = findViewById(R.id.value_ounces_weight_to_height);
        feet = findViewById(R.id.value_feet_weight_to_height);
        inches = findViewById(R.id.value_inches_weight_to_height);
        percentile_answer = findViewById(R.id.value_output_percentile_weight_to_height);
        percentile_10th = findViewById(R.id.value_10th_percentile_weight_to_height);
        percentile_90th = findViewById(R.id.value_90th_percentile_weight_to_height);
        percentile_label = findViewById(R.id.label_percentile_is_weight_to_height);

        //the toggle button that allows you to switch between male and female
        //it just sets "m_f_flag" to the appropriate value to get used in the onClick method for the "calculate" button
        ToggleButton m_f = findViewById(R.id.button_male_female_weight_to_height);
        m_f.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    m_f_flag = 2;                               //toggle is enabled, switched to female
                } else {
                    m_f_flag = 1;                               //toggle is disabled, male (default)
                }
            }
        });

        Switch age = findViewById(R.id.age_switch);
        age.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    age_over_2 = true;                               //toggle is enabled, yes, child is over 2
                } else {
                    age_over_2 = false;                               //toggle is disabled, child is under 2
                }
            }
        });
        //check input and send the correct values to the "CalculateAll" method
        Button calc = findViewById(R.id.button_calculate_weight_to_height);
        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean wt_oz = NumberUtils.isParsable(weight_ounces.getText().toString());
                boolean wt_lbs = NumberUtils.isParsable(weight_pounds.getText().toString());
                double ounces = 0;
                double pounds = 0;
                double centimeters = 0;
                double centimeters_from_feet = 0;
                boolean feet_bool = NumberUtils.isParsable(feet.getText().toString());
                boolean inches_bool = NumberUtils.isParsable(inches.getText().toString());

                //if a valid number for feet
                if(feet_bool){
                    centimeters_from_feet = Double.parseDouble(feet.getText().toString());
                    if(centimeters_from_feet >= 0){
                        centimeters_from_feet = centimeters_from_feet * 12;
                        centimeters_from_feet *= 2.54;
                    }else {centimeters_from_feet = 0;}
                }

                //if a valid number for inches
                if(inches_bool){
                    if(centimeters >= 0){
                        centimeters = Double.parseDouble(inches.getText().toString());
                        centimeters *= 2.54;
                    }else{centimeters = 0;}
                }
                //add the converted feet and inces
                centimeters += centimeters_from_feet;

                //if the age is out of range display a message to the user and exit the method so it doesn't crash
                if(centimeters > 121.5){
                    percentile_label.setTextSize(10);
                    percentile_label.setText("Max Height is 47 Inches");
                    percentile_answer.setText("");
                    return;
                }else {
                    percentile_label.setTextSize(24);
                    percentile_label.setText(getString(R.string.percentile_is ));
                }

                //if a valid number entered for ounces
                if(wt_oz){
                    ounces = Double.parseDouble(weight_ounces.getText().toString());
                    if(ounces >= 0){
                        ounces = ounces / 16;
                    }else {ounces = 0;}
                }

                //if a valid number entered for lbs
                if(wt_lbs){
                    pounds = Double.parseDouble(weight_pounds.getText().toString());
                    if(pounds < 0){pounds = 0;}
                }
                pounds += ounces;
                //lbs to kg
                pounds = pounds * .45359237;
                TableDecision(pounds, centimeters);
            }
        });
    }

    public void TableDecision(double for_wht, double for_cent){
        int percentile;
        Table tbl = new Table(0, 0, 0, 0, 0, 0);
        TableMaker tM = new TableMaker();
        Calculations calc = new Calculations();

        //under 25 months
        if(!age_over_2){
            //male under 2 years
            if(m_f_flag == 1){
                for(Table a : tM.WhoBoysWeightToLength()){
                    if(for_cent >= a.getAge() && for_cent < a.getAge() + .5){
                        tbl = a;
                    }
                }
            }
            //female under 2 years
            if(m_f_flag == 2){
                for(Table b : tM.WhoGirlsWeightToLength()){
                    if(for_cent >= b.getAge() && for_cent < b.getAge() + .5){
                        tbl = b;
                    }
                }
            }
        }
        //over 2 years
        if(age_over_2){
            //25 months until 36 months male
            if(m_f_flag == 1){
                for (Table c : tM.CdcBoysWeightToLength()) {
                    if (for_cent >= c.getAge() - .5 && for_cent < c.getAge() + .5) {
                        tbl = c;
                    }
                }
            }
            //25 months until 36 months female
            if(m_f_flag == 2){
                for (Table d : tM.CdcGirlsWeightToLength()) {
                    if (for_cent >= d.getAge() - .5 && for_cent < d.getAge() + .5) {
                        tbl = d;
                    }
                }
            }
        }
        percentile = calc.CalculateTheZScores(for_wht, tbl.getM(), tbl.getS());
        percentile_answer.setText(Integer.toString(percentile));
        percentile_10th.setText(Float.toString(calc.GetReferencePercentile(tbl.getP10())));
        percentile_90th.setText(Float.toString(calc.GetReferencePercentile(tbl.getP90())));


    }

}
