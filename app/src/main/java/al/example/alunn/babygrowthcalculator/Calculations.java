package al.example.alunn.babygrowthcalculator;


import org.apache.commons.math3.distribution.NormalDistribution;

public class Calculations {

    //calculates the z scores for the CDC only. Overloaded method
    public int CalculateTheZScores(double x_x, double m_m, double s_s){
        double natural_log_of = Math.log(x_x / m_m);
        double z_score = natural_log_of / s_s;
        //int percentile = ZScoreToPercentile(z_score);
        //return percentile;
        return ZScoreToPercentile(z_score);
    }

    //takes the z score and uses "import org.apache.commons.math3.distribution.*;" to get the percentile
    //then it casts it to int because we don't care about .xxxxx percentile
    private int ZScoreToPercentile(double z) {
        NormalDistribution dist = new NormalDistribution();
        double percentile = dist.cumulativeProbability(z) * 100;
        return (int)percentile;
    }

    //converting for weight
    //This method is used when using variable.setText(Float.toString())
    //convert kg to lbs, times 10, cast to int, cast to float, divide by 10.
    //because the original value was a double and caused formatting problems with having a lot
    //of decimal places. This ensures you have one decimal place
    public float GetReferencePercentile(double ref){
        ref *= 2.20462;
        ref *= 10;
        int int_ref = (int)ref;
        float float_ref = (float)int_ref;
        float_ref = float_ref / 10;
        return float_ref;
    }

    //converting for length
    //same as above method
    public float GetReferencePercentileLength(double ref){
        ref *= .393701;
        ref *= 10;
        int int_ref = (int)ref;
        float float_ref = (float)int_ref;
        float_ref = float_ref / 10;
        return float_ref;
    }

    //the units aren't converted in this one
    //same as above method
    public float GetReferencePercentileNoChange(double ref){
        ref *= 10;
        int int_ref = (int)ref;
        float float_ref = (float)int_ref;
        float_ref = float_ref / 10;
        return float_ref;
    }
}
