package al.example.alunn.babygrowthcalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;



public class MainActivity extends AppCompatActivity {
    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ad stuff here
        MobileAds.initialize(this, "ca-app-pub-7858964101775870~7605172832\n");
        mAdView = findViewById(R.id.adView);
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        //AdRequest adRequest = new AdRequest.Builder().build();
        AdRequest request = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();
        mAdView.loadAd(request);


        Button weight_to_age = findViewById(R.id.button_weight_to_age);
        weight_to_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Weight_To_Age.class));
            }
        });

        Button length_to_age = findViewById(R.id.button_length_to_age);
        length_to_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, LengthToAge.class));
            }
        });

        Button weight_to_length = findViewById(R.id.button_weight_to_length);
        weight_to_length.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, WeightToHeight.class));
            }
        });

        Button head_circumference = findViewById(R.id.button_head_circumference);
        head_circumference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HeadCircToAge.class));
            }
        });

        Button bmi_to_age = findViewById(R.id.button_bmi_to_age);
        bmi_to_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, BMIToAge.class));
            }
        });
    }
}
