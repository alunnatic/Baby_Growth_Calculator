package al.example.alunn.babygrowthcalculator;

public class Table {

    double age;
    double l;
    double m;
    double s;
    double p10;
    double p90;

    public double getAge(){return age;}
    public double getL(){return l;}
    public double getM(){return  m;}
    public double getS(){return  s;}
    public double getP10(){return p10;}
    public double getP90(){return p90;}

    public Table(double ag, double l_, double m_, double s_, double p_10, double p_90){
        this.age = ag;
        this.l = l_;
        this.m = m_;
        this.s = s_;
        this.p10 = p_10;
        this.p90 = p_90;
    }
}
