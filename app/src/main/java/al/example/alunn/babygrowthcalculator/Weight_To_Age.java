package al.example.alunn.babygrowthcalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.commons.lang3.math.NumberUtils;

public class Weight_To_Age extends AppCompatActivity {

    //variables
    int m_f_flag = 1;
    EditText age_years;
    EditText age_months;
    EditText weight_pounds;
    EditText weight_ounces;
    TextView percentile_answer;
    TextView percentile_10th;
    TextView percentile_90th;
    TextView percentile_label;
    private AdView mAdView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight__to__age);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //ad stuff
        mAdView1 = findViewById(R.id.adView_weight_to_age);
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest request1 = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();
        mAdView1.loadAd(request1);

        age_years = findViewById(R.id.value_years_old_weight_to_age);
        age_months = findViewById(R.id.value_months_old_weight_to_age);
        weight_pounds = findViewById(R.id.value_pounds_weight_to_age);
        weight_ounces = findViewById(R.id.value_ounces_weight_to_age);
        percentile_answer = findViewById(R.id.value_output_percentile_weight_to_age);
        percentile_10th = findViewById(R.id.value_10th_percentile_weight_to_age);
        percentile_90th = findViewById(R.id.value_90th_percentile_weight_to_age);
        percentile_label = findViewById(R.id.label_percentile_is_weight_to_age);

        //the toggle button that allows you to switch between male and female
        //it just sets "m_f_flag" to the appropriate value to get used in the onClick method for the "calculate" button
        ToggleButton m_f = findViewById(R.id.button_male_female_weight_to_age);
        m_f.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    m_f_flag = 2;                               //toggle is enabled, switched to female
                } else {
                    m_f_flag = 1;                               //toggle is disabled, male (default)
                }
            }
        });

        //check input and send the correct values to the "CalculateAll" method
        Button calc = findViewById(R.id.button_calculate_weight_to_age);
        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean wt_oz = NumberUtils.isParsable(weight_ounces.getText().toString());
                boolean wt_lbs = NumberUtils.isParsable(weight_pounds.getText().toString());
                boolean age_yrs = NumberUtils.isParsable(age_years.getText().toString());
                boolean age_mo = NumberUtils.isParsable(age_months.getText().toString());
                double age_in_months_decimal = 0;
                double ounces = 0;
                double pounds = 0;


                //check if they entered months, if so convert months to a decimal value and add it
                //to the number of years. If no value for age in months, make the age equal to years only
                //if a valid number is in months
                if(age_mo){
                    //if a valid number is in years
                    if(age_yrs){
                        //if years entered is greater than or equal to 0
                        if(Double.parseDouble(age_years.getText().toString()) >= 0){
                            age_in_months_decimal = Double.parseDouble(age_years.getText().toString()) * 12;
                        }
                    }
                    //if months entered is greater than or equal to 0
                    if(Double.parseDouble(age_months.getText().toString()) >= 0){
                        double months_to_decimal = Double.parseDouble(age_months.getText().toString());
                        age_in_months_decimal += months_to_decimal;
                    }
                //if no months entered, use years only
                }else if (age_yrs) {
                    if(Double.parseDouble(age_years.getText().toString()) >= 0)
                    age_in_months_decimal = Double.parseDouble(age_years.getText().toString()) * 12;
                }

                //if the age is out of range display a message to the user and exit the method so it doesn't crash
                if(age_in_months_decimal > 240){
                    percentile_label.setTextSize(10);
                    percentile_label.setText("Age must be between 2 and 20 years");
                    percentile_answer.setText("");
                    return;
                }else {
                    percentile_label.setTextSize(24);
                    percentile_label.setText(getString(R.string.percentile_is ));
                }

                //if a valid number entered for ounces
                if(wt_oz){
                    ounces = Double.parseDouble(weight_ounces.getText().toString());
                    if(ounces >= 0){
                         ounces = ounces / 16;
                    }
                }

                //if a valid number entered for lbs
                if(wt_lbs){
                    pounds = Double.parseDouble(weight_pounds.getText().toString());
                    pounds += ounces;
                }
                //lbs to kg
                pounds = pounds * .45359237;
                TableDecision(age_in_months_decimal, pounds);


            }
        });
    }

    public void TableDecision(double formatted_age, double formatted_weight){
        int percentile;
        Table tbl = new Table(0, 0, 0, 0, 0, 0);
        TableMaker tM = new TableMaker();
        Calculations calc = new Calculations();

        //under 25 months
        if(formatted_age < 25){
            //male under 25 months
            if(m_f_flag == 1){
                for(Table a : tM.WhoBoysWeightToAge()){
                    if(formatted_age >= a.getAge() && formatted_age < a.getAge() + 1){
                        tbl = a;
                    }
                }
            }
            //female under 25 months
            if(m_f_flag == 2){
                for(Table b : tM.WhoGirlsWeightToAge()){
                    if(formatted_age >= b.getAge() && formatted_age < b.getAge() + 1){
                        tbl = b;
                    }
                }
            }
        }
        //25 months until 36 months
        if(formatted_age >= 25 && formatted_age < 36){
            //25 months until 36 months male
            if(m_f_flag == 1){
                for (Table c : tM.CdcMaleWeightToAge()) {
                    if (formatted_age >= c.getAge() - .5 && formatted_age < c.getAge() + .5) {
                        tbl = c;
                    }
                }
            }
            //25 months until 36 months female
            if(m_f_flag == 2){
                for (Table d : tM.CdcGirlsWeightToAge()) {
                    if (formatted_age >= d.getAge() - .5 && formatted_age < d.getAge() + .5) {
                        tbl = d;
                    }
                }
            }
        }
        //36 months to 20 years
        if(formatted_age >= 36){
            //36 months to 20 years male
            if(m_f_flag == 1){
                for (Table e : tM.CdcBoysWeightToAgeOver24Months()) {
                    if (formatted_age >= e.getAge() - .5 && formatted_age < e.getAge() + .5) {
                        tbl = e;
                    }
                }
            }
            //36 months to 20 years female
            if(m_f_flag == 2){
                for (Table f : tM.CdcGirlsWeightToAgeOver24Months()) {
                    if (formatted_age >= f.getAge() - .5 && formatted_age < f.getAge() + .5) {
                        tbl = f;
                    }
                }
            }
        }
        percentile = calc.CalculateTheZScores(formatted_weight, tbl.getM(), tbl.getS());
        percentile_answer.setText(Integer.toString(percentile));
        percentile_10th.setText(Float.toString(calc.GetReferencePercentile(tbl.getP10())));
        percentile_90th.setText(Float.toString(calc.GetReferencePercentile(tbl.getP90())));


    }
}
