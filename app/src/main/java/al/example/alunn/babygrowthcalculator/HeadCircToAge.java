package al.example.alunn.babygrowthcalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.commons.lang3.math.NumberUtils;

public class HeadCircToAge extends AppCompatActivity {

    //variables
    int m_f_flag = 1;
    EditText age_years;
    EditText age_months;
    EditText length_feet;
    EditText length_inches;
    TextView percentile_answer;
    TextView percentile_10th_head;
    TextView percentile_90th_head;
    TextView percentile_label;
    private AdView mAdView4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_head_circ_to_age);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //ad stuff
        mAdView4 = findViewById(R.id.adView_head_circ);
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest request4 = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();
        mAdView4.loadAd(request4);

        age_years = findViewById(R.id.value_years_old_head_circ);
        age_months = findViewById(R.id.value_months_old_head_circ);
        length_feet = findViewById(R.id.value_feet_head_circ);
        length_inches = findViewById(R.id.value_inches_head_circ);
        percentile_answer = findViewById(R.id.value_output_percentile_head_circ);
        percentile_10th_head = findViewById(R.id.value_10th_percentile_head_circ);
        percentile_90th_head = findViewById(R.id.value_90th_percentile_head_circ);
        percentile_label = findViewById(R.id.label_percentile_is_head_circ);

        //the toggle button that allows you to switch between male and female
        //it just sets "m_f_flag" to the appropriate value to get used in the onClick method for the "calculate" button
        ToggleButton m_f = findViewById(R.id.button_male_female_head_circ);
        m_f.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    m_f_flag = 2;                               //toggle is enabled, switched to female
                } else {
                    m_f_flag = 1;                               //toggle is disabled, male (default)
                }
            }
        });

        //check input and send the correct values to the "CalculateAll" method
        Button calc = findViewById(R.id.button_calculate_head_circ);
        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean len_feet = NumberUtils.isParsable(length_feet.getText().toString());
                boolean len_inch = NumberUtils.isParsable(length_inches.getText().toString());
                boolean age_yrs = NumberUtils.isParsable(age_years.getText().toString());
                boolean age_mo = NumberUtils.isParsable(age_months.getText().toString());
                double age_in_months_decimal = 0;
                double inches = 0;
                double feet = 0;


                //check if they entered months, if so convert months to a decimal value and add it
                //to the number of years. If no value for age in months, make the age equal to years only
                //if a valid number is in months
                if(age_mo){
                    //if a valid number is in years
                    if(age_yrs){
                        //if years entered is greater than or equal to 0
                        if(Double.parseDouble(age_years.getText().toString()) >= 0){
                            age_in_months_decimal = Double.parseDouble(age_years.getText().toString()) * 12;
                        }
                    }
                    //if months entered is greater than or equal to 0
                    if(Double.parseDouble(age_months.getText().toString()) >= 0){
                        double months_to_decimal = Double.parseDouble(age_months.getText().toString());
                        age_in_months_decimal += months_to_decimal;
                    }
                    //if no months entered, use years only
                }else if (age_yrs) {
                    if(Double.parseDouble(age_years.getText().toString()) >= 0)
                        age_in_months_decimal = Double.parseDouble(age_years.getText().toString()) * 12;
                }

                //if the age is out of range display a message to the user and exit the method so it doesn't crash
                if(age_in_months_decimal >= 25){
                    percentile_label.setTextSize(10);
                    percentile_label.setText("Age must be between 2 and 20 years");
                    percentile_answer.setText("");
                    return;
                }else {
                    percentile_label.setTextSize(24);
                    percentile_label.setText(getString(R.string.percentile_is ));
                }

                //if a valid number entered for inches
                if(len_inch){
                    inches = Double.parseDouble(length_inches.getText().toString());
                    if(inches < 0){
                        inches = 0;
                    }
                }

                //if a valid number entered for lbs
                if(len_feet){
                    feet = Double.parseDouble(length_feet.getText().toString());
                    if(feet < 0){
                        feet = 0;
                    }else {
                        feet = feet * 12;     //convert feet to inches
                    }
                }
                inches = feet + inches;
                //inches to centimeters
                inches = inches * 2.54;
                TableDecision(age_in_months_decimal, inches);
            }
        });
    }

    public void TableDecision(double formatted_age, double formatted_weight){
        int percentile;
        Table tbl = new Table(0, 0, 0, 0, 0, 0);
        TableMaker tM = new TableMaker();
        Calculations calc = new Calculations();

            //male
            if(m_f_flag == 1){
                for(Table a : tM.WhoBoysHeadCircumference()){
                    if(formatted_age >= a.getAge() && formatted_age < a.getAge() + 1){
                        tbl = a;
                    }
                }
            }
            //female under 25 months
            if(m_f_flag == 2){
                for(Table b : tM.WhoGirlsHeadCircumferenceToAge()){
                    if(formatted_age >= b.getAge() && formatted_age < b.getAge() + 1){
                        tbl = b;
                    }
                }
            }

        percentile = calc.CalculateTheZScores(formatted_weight, tbl.getM(), tbl.getS());
        percentile_answer.setText(Integer.toString(percentile));
        percentile_10th_head.setText(Float.toString(calc.GetReferencePercentileLength(tbl.getP10())));
        percentile_90th_head.setText(Float.toString(calc.GetReferencePercentileLength(tbl.getP90())));
    }








}
